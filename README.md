# Convertir HTML a PDF en Angular 6

A veces, necesitamos proporcionar los datos importantes de una aplicación en forma de un documento como un PDF o una imagen. Para ello es necesario convertir el diseño HTML en un documento. Para ello utilizaremos una librería de JavaScript llamada JSPDF. Se puede leer su documentación oficial  [aquí](https://rawgit.com/MrRio/jsPDF/master/docs/index.html) . 
 
Y también, se ha utilizado otra librería npm llamada html2canvas, que se usa para convertir el HTML en Canvas y luego, poder poner los archivos de imagen en nuestro documento pdf. Para eso, necesitamos descargar sus dependencias.

``` JavaScript
npm install jspdf 
```

![](./imagenes/Selección_102.png)

``` JavaScript
npm instalar html2canvas 
```
![](./imagenes/Selección_103.png)

Cuando hayamos terminado con la parte de instalación, importamos las librerías a nuestro componente.
![](./imagenes/Selección_104.png)

En el archivo html, debemos colocar un id sobre el elemento que se desea mostrar en el pdf en este caso será una tabla.

![](./imagenes/Selección_105.png)

Se ha colocado un botón para generar el PDF de la tabla HTML. Entonces, cuando haga clic en el botón “Imprimir”, se generará un PDF en papel de tamaño A4.

![](./imagenes/Selección_106.png)

Ahora, implementamos la función que va a capturar el contenido del html en el archivo .ts de nuestro componente. En este método llamado capturarContenido (), que genera un documento y se guarda en un sistema local.
 
Para eso, se proporciona algunas configuraciones, como altura de imagen, ancho, margen izquierdo, etc.

![](./imagenes/Selección_107.png)

para comprobar el funcionamiento de nuestra función , accedemos a la vista donde se encuentra la tabla que deseamos convertir a PDF

![](./imagenes/Selección_108.png)

damos clic sobre el botón “descargar” y la tabla se descargara en un archivo pdf.

![](./imagenes/Selección_109.png)

Abrimos el documento que se acaba de descargar y podemos la tabla.

![](./imagenes/Selección_110.png)

<a href="https://www.facebook.com/oscar.sambache" target="_blank"><img alt="Encuentrame en facebook:" height="35" width="35" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTANMlMcb_-pUSuzjnpvSynOA3Ontg9Z2I1NE24WQ_KAk34yYmh"/> Oscar Sambache</a>